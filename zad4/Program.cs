﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad4
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount bankAccount = new BankAccount("Adrian Župarić", "Brodska 31", 2303);
            Memento memento;
            memento = bankAccount.StoreState();
            Console.WriteLine(bankAccount.ToString());
            bankAccount.UpdateBalance(4321);
            Console.WriteLine(bankAccount.ToString());

            Console.WriteLine("Prijašnje stanje: " + memento.Balance);

        }
    }
}
